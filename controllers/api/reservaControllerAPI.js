var Reserva = require("../../models/reserva");

exports.reserva_list = (req, res) => {
  Reserva.allReservas((err, ents) => {
    if (err) console.log(err);
    res.status(200).json({
      reservas: ents,
    });
  });
};
exports.reserva_listByUser = (req, res) => {
  Reserva.allByUser(req.params.userName, (err, ents) => {
    if (err) console.log(err);
    res.status(200).json({
      reservas: ents,
    });
  });
};
exports.reserva_listByBicicleta = (req, res) => {
  Reserva.allByBicicleta(req.params.bicicleta, (err, ents) => {
    if (err) console.log(err);
    res.status(200).json({
      reservas: ents,
    });
  });
};
exports.reserva_create = function (req, res) {
  var ent = new Reserva({
    userName: req.body.userName,
    bicicleta: req.body.bicicleta,
    inicio: req.body.inicio,
    fin: req.body.fin,
    codigo: req.body.codigo,
  });

  Reserva.add(ent, (err, nEnt) => {
    if (err) console.log(err);
    res.status(200).json({
      reserva: nEnt,
    });
  });
};

exports.reserva_delete = function (req, res) {
  Reserva.removeByCodigo(req.params.codigo, (err, dr) => {
    if (err) console.log(err);
    res.status(204).end();
  });
};

exports.reserva_edit = function (req, res) {
  Reserva.findByCodigo(req.params.codigo, (err, targetEnt) => {
    if (err) console.log(err);
    targetEnt.userName = req.body.userName;
    targetEnt.bicicleta = req.body.bicicleta;
    targetEnt.inicio = req.body.inicio;
    targetEnt.fin = req.body.fin;
    Reserva.editByCodigo(req.params.codigo, targetEnt, (err) => {
      if (err) console.log(err);
      res.status(204).send();
    });
  });
};

exports.reserva = function (req, res) {
  Reserva.findByCodigo(req.params.codigo, (err, ent) => {
    if (err) console.log(err);
    if (ent !== null) res.status(200).json(ent);
    else res.sendStatus(404);
  });
};
