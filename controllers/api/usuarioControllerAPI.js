var Usuario = require("../../models/usuario");

exports.usuario_list = (req, res) => {
  Usuario.allUsuario((err, _usuarios) => {
    if (err) console.log(err);
    res.status(200).json({
      usuarios: _usuarios,
    });
  });
};

exports.usuario_create = function (req, res) {
  var ent = new Usuario({
    userName: req.body.userName,
    nombre: req.body.nombre,
    password: req.body.password,
    email: req.body.email,
  });

  Usuario.add(ent, (err, nEnt) => {
    if (err) {
      console.log(err);
      res.status(500).json(err);
    }
    res.status(200).json({
      usuario: nEnt,
    });
  });
};

exports.usuario_delete = function (req, res) {
  Usuario.removeByUserName(req.params.userName, (err, dr) => {
    if (err) console.log(err);
    res.status(204).end();
  });
};

exports.usuario_edit = function (req, res) {
  var ent = new Usuario({
    userName: req.body.userName,
    nombre: req.body.nombre,
  });
  Usuario.findByUserName(req.params.userName, (err, targetEnt) => {
    if (err) console.log(err);
    targetEnt.userName = ent.userName;
    targetEnt.nombre = ent.nombre;
    Usuario.editByUserName(targetEnt.userName, targetEnt, (err) => {
      if (err) console.log(err);
      res.status(204).send();
    });
  });
};

exports.usuario = function (req, res) {
  Usuario.findByUserName(req.params.userName, (err, ent) => {
    if (err) console.log(err);
    if (ent !== null) res.status(200).json(ent);
    else res.sendStatus(404);
  });
};

exports.usuario_reservar = function (req, res) {
  Usuario.findById(req.body.id, (err, usr) => {
    if (err) console.log(err);
    console.log(usr);
    usr.reservar(req.body.bici_id, req.body.inicio, req.body.fin, (err) => {
      console.log("reservado!!!");
      res.status(200).send();
    });
  });
};
