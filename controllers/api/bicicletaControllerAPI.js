var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = (req, res) => {
  Bicicleta.allBicis((err, bicis) => {
    if (err) console.log(err);
    res.status(200).json({
      bicicletas: bicis,
    });
  });
};

exports.bicicleta_create = function (req, res) {
  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng],
  });

  Bicicleta.add(bici, (err, nBici) => {
    if (err) console.log(err);
    res.status(200).json({
      bicicleta: nBici,
    });
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeByCode(req.params.code, (err, dr) => {
    if (err) console.log(err);
    res.status(204).end();
  });
};

exports.bicicleta_edit = function (req, res) {
  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: req.body.ubicacion,
  });
  Bicicleta.findByCode(parseInt(req.params.code), (err, targetBici) => {
    if (err) console.log(err);
    targetBici.code = bici.code;
    targetBici.color = bici.color;
    targetBici.modelo = bici.modelo;
    targetBici.ubicacion = bici.ubicacion;
    Bicicleta.editByCode(targetBici.code, targetBici, (err) => {
      if (err) console.log(err);
      res.status(204).send();
    });
  });
};

exports.bicicleta = function (req, res) {
  Bicicleta.findByCode(parseInt(req.params.code), (err, bici) => {
    if (err) console.log(err);
    if (bici !== null) res.status(200).json(bici);
    else res.sendStatus(404);
  });
};
