var Usuario = require("../models/usuario");
var Token = require("../models/token");
const { model } = require("../models/token");
const usuario = require("../models/usuario");

module.exports = {
  confirmacionGet: (req, res, next) => {
    Token.findOne({ token: req.params.token }, (err, token) => {
      if (!token)
        return res.status(400).send({
          type: "not-virified",
          msg: "No se pudo verificar la cuenta",
        });
      Usuario.findById(token._userId, function (err, usuario) {
        if (!usuario) {
          return res.status(400).send({ msg: "No encontramos el usuario con este token" });
        }
        if (usuario.verificado) { return res.redirectt("/usuarios"); }
        usuario.verificado = true;
        usuario.save((err) => {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }
          res.redirect("/");
        });
      });
    });
  },
};
