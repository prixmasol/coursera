var Reserva = require("../models/reserva");

exports.reserva_list = function (req, res) {
  Usuario.allReservas((err, reservas) => {
    if (err) console.log;
    res.render("reservas/index", { reservas });
  });
};
exports.reserva_userName = function (req, res) {
  Usuario.findByUserName(req.params.userName, (err, reservas) => {
    res.render("reservas/byuser", { reservas });
  });
};
exports.reserva_bicicleta = function (req, res) {
  Usuario.findByUserName(req.params.bicicleta, (err, reservas) => {
    res.render("reservas/bybicicleta", { reservas });
  });
};

exports.reserva_view = function (req, res) {
  Usuario.findByCodigo(req.params.codigo, (err, reserva) => {
    if (err) console.log(err);
    res.render("reservas/view", { reserva });
  });
};

exports.reserva_create_get = function (req, res) {
  res.render("reservas/create");
};

exports.reserva_create_post = function (req, res) {
  var ent = new Reserva({
    userName: req.body.userName,
    bicicleta: req.body.bicicleta,
    inicio: req.body.inicio,
    fin: req.body.fin,
    codigo: req.body.codigo,
  });
  Reserva.add(ent, (err) => {
    if (err) console.log(err);
    res.redirect("/reservas");
  });
};

exports.reserva_delete_post = function (req, res) {
  Usuario.removeByCodigo(req.body.codigo, (err) => {
    if (err) console.log(err);
    res.redirect("/reservas");
  });
};

exports.reserva_edit_get = function (req, res) {
  Usuario.findByCodigo(req.params.codigo, (err, reserva) => {
    res.render("reservas/edit", { reserva });
  });
};

exports.reserva_edit_post = function (req, res) {
  Reserva.findByUserName(req.params.codigo, (err, reserva) => {
    reserva.bicicleta = req.body.bicicleta;
    reserva.userName = req.body.userName;
    reserva.inicio = req.body.inicio;
    reserva.fin = req.body.fin;
    Reserva.editByCodigo(req.params.codigo, reserva, (err) => {
      if (err) console.log(err);
      res.redirect("/reservas");
    });
  });
};
