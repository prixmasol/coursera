var Bicicleta = require("../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis((err, bicis) => {
    res.render("bicicletas/index", { bicis: bicis });
  });
};

exports.bicicleta_view = function (req, res) {
  Bicicleta.findById(req.params.id, (err, bici) => {
    res.render("bicicletas/view", { bici });
  });
};

exports.blicicleta_create_get = function (req, res) {
  res.render("bicicletas/create");
};

exports.blicicleta_create_post = function (req, res) {
  var bici = new Bicicleta({
    code: req.body.id,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng],
  });
  Bicicleta.add(bici, (err, aBici) => {
    if (err) console.log(err);
    res.redirect("/bicicletas");
  });
};

exports.bicicleta_delete_post = function (req, res) {
  Bicicleta.removeById(req.body.id);

  res.redirect("/bicicletas");
};

exports.blicicleta_edit_get = function (req, res) {
  var bici = Bicicleta.findById(req.params.id);
  res.render("bicicletas/edit", { bici });
};

exports.blicicleta_edit_post = function (req, res) {
  var bici = Bicicleta.findById(req.params.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];

  res.redirect("/bicicletas");
};
