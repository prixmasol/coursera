var Usuario = require("../models/usuario");

module.exports = {
  list: function (req, res) {
    Usuario.allUsuario((err, usuarios) => {
      if (err) console.log;
      res.render("usuarios/index", { usuarios });
    });
  },
  view: function (req, res) {
    Usuario.findByUserName(req.params.id, (err, usuario) => {
      if (err) console.log(err);
      console.log(usuario);
      res.render("usuarios/view", { usuario: usuario });
    });
  },
  create_get: function (req, res) {
    res.render("usuarios/create", { errors: {}, usuario: new Usuario() });
  },
  create_post: function (req, res) {
    if (req.body.password != req.body.confirm_password) {
      res.render("usuarios/create", {
        errors: {
          confirm_password: { message: "No coinciden los password" },
          password: { message: "No coinciden los password" },
        },
        usuario: new Usuario({
          userName: req.body.userName,
          nombre: req.body.nombre,
          email: req.body.email,
        }),
      });
      return;
    }
    Usuario.create(
      {
        userName: req.body.userName,
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password,
      },
      (err, nuevoUsuario) => {
        if (err) {
          console.log(err);
          res.render("usuarios/create", {
            errors: err.errors,
            usuario: new Usuario({
              userName: req.body.userName,
              nombre: req.body.nombre,
              email: req.body.email,
            }),
          });
          return;
        }
        nuevoUsuario.enviar_email_bienvenida();
        res.redirect("/usuarios");
      }
    );
  },
  delete_post: function (req, res) {
    Usuario.findByIdAndRemove(req.params.id, (err) => {
      if (err) console.log(err);
      res.redirect("/usuarios");
    });
  },
  edit_get: function (req, res) {
    Usuario.findById(req.params.id, (err, usuario) => {
      if (err) console.log;
      res.render("usuarios/edit", { errors: {}, usuario: usuario });
    });
  },
  edit_post: function (req, res) {
    let update_values = { nombre: req.body.nombre };
    Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
      if (err) {
        console.log(err);
        res.render(`usuarios/edit`, {
          errors: err.errors,
          usuario: new Usuario({
            nombre: req.body.nombre,
            userName: req.body.userName,
            email: req.body.email,
          }),
        });
      } else {
        res.redirect("/usuarios");
        return;
      }
    });
  },
};
