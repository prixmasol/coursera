const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");

let mailConfig;
let options;
switch (process.env.NODE_ENV0) {
  case "production":
    options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET,
      },
    };
    mailConfig = sgTransport(options);
    break;
  case "staging":
    options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET,
      },
    };
    mailConfig = sgTransport(options);
    break;
  default:
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: process.env.ethereal_username,
        pass: process.env.ethereal_password,
      },
    };
    break;
}

module.exports = nodemailer.createTransport(mailConfig);
