var Usuario = require("../../models/usuario");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");
var url = "http://localhost:3000/api/usuarios";

describe("Usuario API - ", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });
  beforeEach(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";
      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      mongoose.set("useCreateIndex", true);
      const db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB Connection error: "));
      db.once("open", function () {
        console.log("Nos conectamos con la bd test");
        done();
      });
    });
  });
  afterEach(function (done) {
    Usuario.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  it("Status 200 - Get All", (done) => {
    request.get(url, (err, res, req) => {
      expect(res.statusCode).toBe(200);
      expect(JSON.parse(res.body).usuarios.length).toBe(0);
      done();
    });
  });
  it("Crear Usuario", (done) => {
    var headers = { "content-type": "application/json" };
    var ent = ' {"userName": "pospino","nombre": "Pedro" } ';

    request.post(
      {
        url: url,
        headers: headers,
        body: ent,
      },
      function (err, res, req) {
        if (err) console.log(err);
        expect(res.statusCode).toBe(200);
        request.get(url, (err, res, req) => {
          expect(res.statusCode).toBe(200);

          expect(JSON.parse(res.body).usuarios.length).toBe(1);
          let ent = JSON.parse(res.body).usuarios[0];
          expect(ent.nombre).toBe("Pedro");
          done();
        });
      }
    );
  });

  it("Buscar Un Usuario", (done) => {
    var headers = { "content-type": "application/json" };
    var ent = ' {"userName": "pospino","nombre": "Pedro" } ';

    request.post(
      {
        url: url,
        headers: headers,
        body: ent,
      },
      function (err, res, req) {
        if (err) console.log(err);
        expect(res.statusCode).toBe(200);
        request.get(url, (err, res, req) => {
          expect(res.statusCode).toBe(200);
          expect(JSON.parse(res.body).usuarios.length).toBe(1);
          request.get(url + "/pospino", (err, res, req) => {
            expect(res.statusCode).toBe(200);
            let ent = JSON.parse(res.body);
            expect(ent.nombre).toBe("Pedro");
            done();
          });
        });
      }
    );
  });
  it("Actualizar Usuario", (done) => {
    var headers = { "content-type": "application/json" };
    var ent = ' {"userName": "pospino","nombre": "Pedro" } ';

    request.post(
      {
        url: url,
        headers: headers,
        body: ent,
      },
      function (err, res, req) {
        if (err) console.log(err);
        expect(res.statusCode).toBe(200);
        request.get(url, (err, res, req) => {
          if (err) console.log(err);
          expect(res.statusCode).toBe(200);
          expect(JSON.parse(res.body).usuarios.length).toBe(1);
          let ents = JSON.parse(res.body).usuarios;
          expect(ents[0].nombre).toBe("Pedro");
          var headers = { "content-type": "application/json" };
          var ent = ' { "userName":"pospino","nombre": "Pedro Antonio" } ';
          request.put(
            {
              url: url + "/pospino",
              headers: headers,
              body: ent,
            },
            function (err, res, bod) {
              expect(res.statusCode).toBe(204);
              request.get(url, (err, res, req) => {
                expect(res.statusCode).toBe(200);
                expect(JSON.parse(res.body).usuarios[0].nombre).toBe(
                  "Pedro Antonio"
                );
                done();
              });
            }
          );
        });
      }
    );
  });
  it("Borrar Usuario", (done) => {
    request.get(url, (err, res, req) => {
      expect(res.statusCode).toBe(200);
      expect(JSON.parse(res.body).usuarios.length).toBe(0);

      var headers = { "content-type": "application/json" };
      var ent = ' {"userName": "pospino","nombre": "Pedro" } ';
      request.post(
        {
          url: url,
          headers: headers,
          body: ent,
        },
        function (err, res) {
          if (err) console.log(err);
          expect(res.statusCode).toBe(200);
          request.get(url, (err, res, req) => {
            if (err) console.log(err);
            expect(res.statusCode).toBe(200);
            expect(JSON.parse(res.body).usuarios.length).toBe(1);
            let ents = JSON.parse(res.body).usuarios;
            expect(ents[0].nombre).toBe("Pedro");
            request.delete(url + "/pospino", function (req, res) {
              expect(res.statusCode).toBe(204);
              request.get(url + "/pospino", (err, res, req) => {
                expect(res.statusCode).toBe(404);
                done();
              });
            });
          });
        }
      );
    });
  });
});
