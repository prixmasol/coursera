var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");

describe("Bicicleta API - ", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });
  beforeEach(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";
      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      mongoose.set("useCreateIndex", true);
      const db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB Connection error: "));
      db.once("open", function () {
        console.log("Nos conectamos con la bd test");
        done();
      });
    });
  });
  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  it("Status 200 - Get All", (done) => {
    request.get("http://localhost:3000/api/bicicletas", (err, res, req) => {
      expect(res.statusCode).toBe(200);
      expect(JSON.parse(res.body).bicicletas.length).toBe(0);
      done();
    });
  });
  it("Crear Bicicleta", (done) => {
    var headers = { "content-type": "application/json" };
    var aBici =
      ' {"code": 10,"color": "rojo","modelo": "Urbana","lat": -32.98184263945343,"lng": -68.78846167201478 } ';

    request.post(
      {
        url: "http://localhost:3000/api/bicicletas",
        headers: headers,
        body: aBici,
      },
      function (err, res, req) {
        if (err) console.log(err);
        expect(res.statusCode).toBe(200);
        request.get("http://localhost:3000/api/bicicletas", (err, res, req) => {
          expect(res.statusCode).toBe(200);

          expect(JSON.parse(res.body).bicicletas.length).toBe(1);
          let aBici = JSON.parse(res.body).bicicletas[0];
          expect(aBici.color).toBe("rojo");
          expect(aBici.code).toBe(10);
          done();
        });
      }
    );
  });

  it("Buscar Una Bicicleta", (done) => {
    var headers = { "content-type": "application/json" };
    var aBici =
      ' {"code": 10,"color": "rojo","modelo": "Urbana","lat": -32.98184263945343,"lng": -68.78846167201478 } ';

    request.post(
      {
        url: "http://localhost:3000/api/bicicletas",
        headers: headers,
        body: aBici,
      },
      function (err, res, req) {
        if (err) console.log(err);
        expect(res.statusCode).toBe(200);
        request.get("http://localhost:3000/api/bicicletas", (err, res, req) => {
          expect(res.statusCode).toBe(200);
          expect(JSON.parse(res.body).bicicletas.length).toBe(1);
          request.get(
            "http://localhost:3000/api/bicicletas/10",
            (err, res, req) => {
              expect(res.statusCode).toBe(200);
              let aBici = JSON.parse(res.body);
              expect(aBici.code).toBe(10);
              expect(aBici.color).toBe("rojo");
              done();
            }
          );
        });
      }
    );
  });
  it("Actualizar Bicicleta", (done) => {
    var headers = { "content-type": "application/json" };
    var aBici =
      ' {"code": 10,"color": "rojo","modelo": "Urbana","lat": -32.98184263945343,"lng": -68.78846167201478 } ';

    request.post(
      {
        url: "http://localhost:3000/api/bicicletas",
        headers: headers,
        body: aBici,
      },
      function (err, res, req) {
        if (err) console.log(err);
        expect(res.statusCode).toBe(200);
        request.get("http://localhost:3000/api/bicicletas", (err, res, req) => {
          if (err) console.log(err);
          expect(res.statusCode).toBe(200);
          expect(JSON.parse(res.body).bicicletas.length).toBe(1);
          let bicis = JSON.parse(res.body).bicicletas;
          expect(bicis[0].color).toBe("rojo");
          expect(bicis[0].code).toBe(10);
          var headers = { "content-type": "application/json" };
          var aBici =
            ' { "ubicacion":[-32.98,-68.78],"code":10,"color":"azul","modelo":"Urbana" } ';
          request.put(
            {
              url: "http://localhost:3000/api/bicicletas/10",
              headers: headers,
              body: aBici,
            },
            function (err, res, bod) {
              expect(res.statusCode).toBe(204);
              request.get(
                "http://localhost:3000/api/bicicletas",
                (err, res, req) => {
                  expect(res.statusCode).toBe(200);
                  expect(JSON.parse(res.body).bicicletas[0].color).toBe("azul");
                  done();
                }
              );
            }
          );
        });
      }
    );
  });
  it("Borrar Bicicleta", (done) => {
    request.get("http://localhost:3000/api/bicicletas", (err, res, req) => {
      expect(res.statusCode).toBe(200);
      expect(JSON.parse(res.body).bicicletas.length).toBe(0);

      var headers = { "content-type": "application/json" };
      var aBici =
        ' {"code": 10,"color": "rojo","modelo": "Urbana","lat": -32.98184263945343,"lng": -68.78846167201478 } ';
      request.post(
        {
          url: "http://localhost:3000/api/bicicletas",
          headers: headers,
          body: aBici,
        },
        function (err, res) {
          if (err) console.log(err);
          expect(res.statusCode).toBe(200);
          request.get(
            "http://localhost:3000/api/bicicletas",
            (err, res, req) => {
              if (err) console.log(err);
              expect(res.statusCode).toBe(200);
              expect(JSON.parse(res.body).bicicletas.length).toBe(1);
              let bicis = JSON.parse(res.body).bicicletas;
              expect(bicis[0].color).toBe("rojo");
              expect(bicis[0].code).toBe(10);
              request.delete(
                "http://localhost:3000/api/bicicletas/10",
                function (req, res) {
                  expect(res.statusCode).toBe(204);
                  request.get(
                    "http://localhost:3000/api/bicicletas/10",
                    (err, res, req) => {
                      expect(res.statusCode).toBe(404);
                      done();
                    }
                  );
                }
              );
            }
          );
        }
      );
    });
  });
});
