var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing Bicicletas", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });
  beforeEach(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";
      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      mongoose.set("useCreateIndex", true);
      const db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB Connection error: "));
      db.once("open", function () {
        console.log("Nos conectamos con la bd test");
        done();
      });
    });
  });
  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });
  describe("Bicicleta createInstance", () => {
    it("Crear una instancia del objeto bicicleta", (done) => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [
        11.0091009,
        -74.8291048,
      ]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toBe(11.0091009);
      expect(bici.ubicacion[1]).toBe(-74.8291048);
      done();
    });
  });
  describe("Bicicleta.allBicis", () => {
    it("Comienza Vacia", function (done) {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });
  describe("Bicicleta.add", () => {
    it("Agregando Una Bici", function (done) {
      var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
      Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);

          done();
        });
      });
    });
  });
  describe("Bicicleta.findByCode", () => {
    it("Bicicleta con codigo 1", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);

          var aBici2 = new Bicicleta({
            code: 2,
            color: "rojo",
            modelo: "urbana",
          });
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.log(err);
            Bicicleta.findByCode(1, function (error, targetBici) {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });
  describe("Bicicleta.editByCode", () => {
    it("Bicicleta con Codigo 10", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({
          code: 10,
          color: "verde",
          modelo: "urbana",
          ubicacion: [11.0091009, -74.8291048],
        });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);
          Bicicleta.findByCode(10, (error, targetBici) => {
            targetBici.color = "azul";
            Bicicleta.editByCode(10, targetBici, (err) => {
              if (err) console.log(err);
              Bicicleta.findByCode(10, (error, targetBici) => {
                expect(targetBici.color).toBe("azul");
                done();
              });
            });
          });
        });
      });
    });
  });
  describe("Bicicleta.removeByCode", () => {
    it("Quitar una Bicicleta con codigo 10", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({
          code: 10,
          color: "verde",
          modelo: "urbana",
          ubicacion: [11.0091009, -74.8291048],
        });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);
          Bicicleta.findByCode(10, (error, targetBici) => {
            targetBici.color = "azul";
            Bicicleta.editByCode(10, targetBici, (err) => {
              if (err) console.log(err);
              Bicicleta.findByCode(10, (error, targetBici) => {
                expect(targetBici.color).toBe("azul");
                Bicicleta.removeByCode(10, (error) => {
                  if (error) console.log(error);
                  Bicicleta.findByCode(10, (error, targetBici2) => {
                    expect(targetBici2).toBe(null);
                    done();
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});

/*
beforeEach(() => {
  Bicicleta.allBicis = [];
  console.log("testeando...");
});

describe("Bicicleta.allBicis", () => {
  it("Comienza vacia", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe("Bicicleta.add", () => {
  it("Agregando Una", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, "rojo", "urbana", [11.0091009, -74.8291048]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe("Bicicleta.findById", () => {
  it("Debe devolver la bici con id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, "amarillo", "urbana", [11.0091009, -74.8291048]);
    var b = new Bicicleta(2, "azul", "urbana", [11.0003001, -74.8290041]);

    Bicicleta.add(a);
    Bicicleta.add(b);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(a.id);
    expect(targetBici.color).toBe(a.color);
  });
});

describe("Bicicleta.removeById", () => {
  it("Debe remover la bici con id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, "amarillo", "urbana", [11.0091009, -74.8291048]);
    var b = new Bicicleta(2, "azul", "urbana", [11.0003001, -74.8290041]);

    Bicicleta.add(a);
    Bicicleta.add(b);

    Bicicleta.removeById(1);

    expect(Bicicleta.findById(1)).toBe(null);
  });
});*/
