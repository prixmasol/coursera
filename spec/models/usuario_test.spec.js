var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var Usuario = require("../../models/usuario");
var Reserva = require("../../models/reserva");

describe("Testing Usuarios", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });
  beforeEach(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";
      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      mongoose.set("useCreateIndex", true);
      const db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB Connection error: "));
      db.once("open", function () {
        console.log("Nos conectamos con la bd test");
        done();
      });
    });
  });
  afterEach(function (done) {
    Reserva.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      Usuario.deleteMany({}, (err, success) => {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, (err, success) => {
          if (err) console.log(err);
          done();
        });
      });
    });
  });
  describe("Usuario createInstance", () => {
    it("Crear una instancia del objeto usuario", (done) => {
      var ent = Usuario.createInstance("pospino", "Pedro");

      expect(ent.userName).toBe("pospino");
      expect(ent.nombre).toBe("Pedro");
      done();
    });
  });
  describe("Usuario.allUsuario", () => {
    it("Comienza Vacia", function (done) {
      Usuario.allUsuario(function (err, ents) {
        expect(ents.length).toBe(0);
        done();
      });
    });
  });
  describe("Usuario.add", () => {
    it("Agregando Un Usuario", function (done) {
      var ent = new Usuario({
        userName: "pospino",
        nombre: "Pedro",
      });
      Usuario.add(ent, function (err, newEnt) {
        if (err) console.log(err);
        Usuario.allUsuario(function (err, ents) {
          expect(ents.length).toEqual(1);
          expect(ents[0].userName).toEqual(ent.userName);
          done();
        });
      });
    });
  });
  describe("Usuario.findByUserName", () => {
    it("Usuario con codigo pospino", (done) => {
      Usuario.allUsuario((err, ents) => {
        expect(ents.length).toBe(0);
        var ent = new Usuario({
          userName: "pospino",
          nombre: "Pedro",
        });
        Usuario.add(ent, function (err, newEnt) {
          if (err) console.log(err);

          var ent2 = new Usuario({
            userName: "svalega",
            nombre: "Sandra",
          });
          Usuario.add(ent2, function (err, newEnt) {
            if (err) console.log(err);
            Usuario.findByUserName("pospino", function (error, targetEnt) {
              expect(targetEnt.userName).toBe(ent.userName);
              expect(targetEnt.nombre).toBe(ent.nombre);
              done();
            });
          });
        });
      });
    });
  });
  describe("Usuario.editByUserName", () => {
    it("Usuario con Codigo pospino", (done) => {
      Usuario.allUsuario((err, ents) => {
        expect(ents.length).toBe(0);
        var ent = new Usuario({
          userName: "pospino",
          nombre: "Pedro",
        });
        Usuario.add(ent, function (err, newEnt) {
          if (err) console.log(err);
          Usuario.findByUserName("pospino", (error, targetEnt) => {
            targetEnt.nombre = "Pedro Antonio";
            Usuario.editByUserName("pospino", targetEnt, (err) => {
              if (err) console.log(err);
              Usuario.findByUserName("pospino", (error, targetEnt) => {
                expect(targetEnt.nombre).toBe("Pedro Antonio");
                done();
              });
            });
          });
        });
      });
    });
  });
  describe("Usuario.removeByUserName", () => {
    it("Quitar una Usuario con codigo pospino", (done) => {
      Usuario.allUsuario((err, ents) => {
        expect(ents.length).toBe(0);
        var ent = new Usuario({
          userName: "pospino",
          nombre: "Pedro",
        });
        Usuario.add(ent, function (err, newEnt) {
          if (err) console.log(err);
          Usuario.removeByUserName("pospino", (error) => {
            if (error) console.log(error);
            Usuario.findByUserName("pospino", (error, target) => {
              expect(target).toBe(null);
              done();
            });
          });
        });
      });
    });
  });
  describe("Cuando un usuario reserve una bici", () => {
    it("debe existir la reserva", (done) => {
      const usuario = new Usuario({ userName: "pospino", nombre: "Pedro" });
      usuario.save();
      const bicicleta = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
      });
      bicicleta.save();

      var hoy = new Date();
      var mañana = new Date();
      mañana.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta.id, hoy, mañana, (err, reserva) => {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec((err, reservas) => {
            if (err) console.log(err);
            expect(reservas.length).toBe(1);
            expect(reservas[0].diasDeReserva()).toBe(2);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe("Pedro");
            done();
          });
      });
    });
  });
});

/*
beforeEach(() => {
  Usuario.allBicis = [];
  console.log("testeando...");
});

describe("Usuario.allBicis", () => {
  it("Comienza vacia", () => {
    expect(Usuario.allBicis.length).toBe(0);
  });
});

describe("Usuario.add", () => {
  it("Agregando Una", () => {
    expect(Usuario.allBicis.length).toBe(0);
    var a = new Usuario(1, "rojo", "urbana", [11.0091009, -74.8291048]);
    Usuario.add(a);
    expect(Usuario.allBicis.length).toBe(1);
    expect(Usuario.allBicis[0]).toBe(a);
  });
});

describe("Usuario.findById", () => {
  it("Debe devolver la bici con id 1", () => {
    expect(Usuario.allBicis.length).toBe(0);
    var a = new Usuario(1, "amarillo", "urbana", [11.0091009, -74.8291048]);
    var b = new Usuario(2, "azul", "urbana", [11.0003001, -74.8290041]);

    Usuario.add(a);
    Usuario.add(b);

    var targetBici = Usuario.findById(1);
    expect(targetBici.id).toBe(a.id);
    expect(targetBici.color).toBe(a.color);
  });
});

describe("Usuario.removeById", () => {
  it("Debe remover la bici con id 1", () => {
    expect(Usuario.allBicis.length).toBe(0);
    var a = new Usuario(1, "amarillo", "urbana", [11.0091009, -74.8291048]);
    var b = new Usuario(2, "azul", "urbana", [11.0003001, -74.8290041]);

    Usuario.add(a);
    Usuario.add(b);

    Usuario.removeById(1);

    expect(Usuario.findById(1)).toBe(null);
  });
});*/
