var express = require("express");
var router = express.Router();
var entController = require("../controllers/reserva");

router.get("/", entController.reserva_list);
router.get("/:userName/byuser/", entController.reserva_userName);
router.get("/:bicicleta/bybicicleta/", entController.reserva_bicicleta);
router.get("/create", entController.reserva_create_get);
router.post("/create", entController.reserva_create_post);
router.post("/:codigo/delete", entController.reserva_delete_post);
router.get("/:codigo/edit", entController.reserva_edit_get);
router.post("/:codigo/edit", entController.reserva_edit_post);
router.get("/:codigo/view", entController.reserva_view);

module.exports = router;
