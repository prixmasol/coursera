var express = require("express");
var router = express.Router();
var bicicletaController = require("../controllers/bicicleta");

router.get("/", bicicletaController.bicicleta_list);
router.get("/create", bicicletaController.blicicleta_create_get);
router.post("/create", bicicletaController.blicicleta_create_post);
router.post("/:id/delete", bicicletaController.bicicleta_delete_post);
router.get("/:id/edit", bicicletaController.blicicleta_edit_get);
router.post("/:id/edit", bicicletaController.blicicleta_edit_post);
router.get("/:id/view", bicicletaController.bicicleta_view);

module.exports = router;
