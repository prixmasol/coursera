var express = require("express");
var router = express.Router();
var usuarioController = require("../controllers/usuario");

router.get("/", usuarioController.list);
router.get("/create", usuarioController.create_get);
router.post("/create", usuarioController.create_post);
router.post("/:id/delete", usuarioController.delete_post);
router.get("/:id/edit", usuarioController.edit_get);
router.post("/:id/edit", usuarioController.edit_post);
router.get("/:id/view", usuarioController.view);

module.exports = router;
