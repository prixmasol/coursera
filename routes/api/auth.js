const express = require("express");
const passport = require("passport");
const router = express.Router();

const controller = require("../../controllers/api/authControllerAPI");

router.post("/authenticate", controller.authenticate);
router.post("/forgotPassword", controller.forgotPassword);
router.post(
  "/facebook_token",
  passport.authenticate("facebook-token"),
  controller.authFacebookToken
);
module.exports = router;
