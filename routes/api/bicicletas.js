var express = require("express");
var router = express.Router();
var bicicletaController = require("../../controllers/api/bicicletaControllerAPI");

router.get("/", bicicletaController.bicicleta_list);
router.post("/", bicicletaController.bicicleta_create);
router.delete("/:code", bicicletaController.bicicleta_delete);
router.put("/:code", bicicletaController.bicicleta_edit);
router.get("/:code", bicicletaController.bicicleta);

module.exports = router;
