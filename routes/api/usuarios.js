var express = require("express");
var router = express.Router();
var entController = require("../../controllers/api/usuarioControllerAPI");

router.get("/", entController.usuario_list);
router.post("/", entController.usuario_create);
router.delete("/:userName", entController.usuario_delete);
router.put("/:userName", entController.usuario_edit);
router.get("/:userName", entController.usuario);

module.exports = router;
