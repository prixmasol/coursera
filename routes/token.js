var express = require("express");
var router = express.Router();
var controller = require("../controllers/token");

router.get("/confirmation/:token", controller.confirmacionGet);

module.exports = router;
