var map = L.map("main_map").setView([11.0003009, -74.8290048], 13);

L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicG9zcGlubyIsImEiOiJja2J2NGpwZ3MwMmppMnRwM3V5eTJvY2JoIn0.TXPYzDux_sQzO0wQZxHGCg",
  {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
    accessToken: "your.mapbox.access.token",
  }
).addTo(map);

$.ajax({
  dataType: "json",
  url: "/api/bicicletas",
  success: function (data) {
    console.log(data);
    data.bicicletas.forEach(function (bici) {
      L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
    });
  },
});
