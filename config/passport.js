const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
var GoogleStrategy = require("passport-google-oauth20").Strategy;
var FacebookTokenStrategy = require("passport-facebook-token");
const Usuario = require("../models/usuario");

passport.use(
  new LocalStrategy(function (email, password, done) {
    Usuario.findOne({ email: email }, function (err, usr) {
      if (err) return done(err);
      if (!usr)
        return done(null, false, {
          message: "Email no existe",
        });
      if (!usr.validPassword(password))
        return done(null, false, {
          message: "Contraseña invalida",
        });
      if (!usr.verificado)
        return done(null, false, {
          message: "Usuario no ha sido verificado",
        });

      return done(null, usr);
    });
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: "/auth/google/callback",
    },
    function (accessToken, refreshToken, profile, cb) {
      console.log(profile);
      Usuario.findOrCreateByGoogle(profile, function (err, user) {
        return cb(err, user);
      });
    }
  )
);

passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
    },
    function (accessToken, refreshToken, profile, cb) {
      console.log(profile);
      try {
        Usuario.findOrCreateByFacebook(profile, function (err, user) {
          return cb(err, user);
        });
      } catch (err2) {
        console.log(err2);
        return cb(err2, null);
      }
    }
  )
);

passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  Usuario.findById(id, function (err, usuario) {
    cb(err, usuario);
  });
});

module.exports = passport;
