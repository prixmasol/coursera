var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: "2dsphere", sparse: true },
  },
});

BicicletaSchema.methods.toString = function () {
  return `code: ${this.code} | color: ${this.color}`;
};

BicicletaSchema.statics.createInstance = function (
  code,
  color,
  modelo,
  ubicacion
) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion,
  });
};

BicicletaSchema.statics.allBicis = function (cb) {
  return this.find({}, cb);
};

BicicletaSchema.statics.add = function (aBici, cb) {
  this.create(aBici, cb);
};

BicicletaSchema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
};

BicicletaSchema.statics.editByCode = function (aCode, aBici, cb) {
  return this.updateOne({ code: aCode }, aBici, cb);
};

BicicletaSchema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model("Bicicleta", BicicletaSchema);

/*
var Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};


Bicicleta.prototype.toString = function () {
  return `id: ${this.id}+ | color: ${this.color}`;
};

Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
  Bicicleta.allBicis.push(aBici);
};

Bicicleta.findById = function (aBiciId) {
  var aBici = Bicicleta.allBicis.find((a) => a.id == aBiciId);
  if (aBici) return aBici;
  else return null;
};

Bicicleta.removeById = function (aBiciId) {
  for (let i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == aBiciId) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
};

var a = new Bicicleta(1, "rojo", "urbana", [11.0091009, -74.8291048]);
var b = new Bicicleta(2, "azul", "urbana", [11.0003001, -74.8290041]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;*/
