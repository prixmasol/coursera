var mongoose = require("mongoose");
var Reserva = require("./reserva");
var Token = require("./token");
var bcrypt = require("bcrypt");
var crypto = require("crypto");
var uniqueValidator = require("mongoose-unique-validator");

var Schema = mongoose.Schema;

const saltRounds = 10;
const validateEmail = function (email) {
  const re = /^\w+([\.-]?\w)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};
const mailer = require("../mailer/mailer");
const { Console } = require("console");
var UsuarioSchema = new Schema({
  userName: {
    type: String,
    trim: true,
    required: [true, "El Usuario es Obligatorio"],
    unique: true,
  },
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es Obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El email es Obligatorio"],
    lowercase: true,
    validate: [validateEmail, "Por favor ingrese un email valido"],
    match: [/^\w+([\.-]?\w)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
    unique: true,
  },
  password: {
    type: String,
    required: [true, "El password es Obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String,
});

UsuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

UsuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

UsuarioSchema.plugin(uniqueValidator, {
  message: " El {PATH} ya existe con otro usuario ",
});

UsuarioSchema.methods.toString = function () {
  return `Usuario: ${this.userName} | nombre: ${this.nombre}`;
};

UsuarioSchema.statics.createInstance = function (_userName, _nombre) {
  return new this({
    userName: _userName,
    nombre: _nombre,
  });
};

UsuarioSchema.statics.allUsuario = function (cb) {
  return this.find({}, cb);
};
UsuarioSchema.statics.add = function (_usuario, cb) {
  this.create(_usuario, cb);
};
UsuarioSchema.statics.findByUserName = function (_userName, cb) {
  return this.findOne({ userName: _userName }, cb);
};

UsuarioSchema.statics.editByUserName = function (_userName, _Usuario, cb) {
  return this.updateOne({ userName: _userName }, _Usuario, cb);
};
UsuarioSchema.statics.removeByUserName = function (_userName, cb) {
  return this.deleteOne({ userName: _userName }, cb);
};

UsuarioSchema.methods.reservar = function (biciId, inicio, fin, cb) {
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    inicio: inicio,
    fin: fin,
  });

  reserva.save(cb);
};

UsuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save((err) => {
    if (err) {
      return console.log(err.message);
    }
    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Verificación de cuenta",
      text: `Hola,\n\nPor favor, para verificar su cuenta haga click en el siguiente enlace\nhttp://localhost:3000/token/confirmation/${token.token}`,
    };
    mailer.sendMail(mailOptions, (err) => {
      if (err) {
        return console.log(err.message);
      }
      console.log(
        `Un correo de verificación se ha enviado a ${email_destination}.`
      );
    });
  });
};

UsuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }
    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Reseteo de password",
      text:
        "Hola, \n\n" +
        "Por favor, para resetear el password de su cuenta haga click en este link: \n" +
        "http://localhost:3000" +
        "/resetPassword/" +
        token.token +
        " \n",
    };
    mailer.sendMail(mailOptions, function (err) {
      if (err) return cb(err);
      console.log("Se envio el email de reseteo a " + email_destination);
    });
    cb(null);
  });
};

UsuarioSchema.statics.findOrCreateByGoogle = function findOneOrCreate(
  condition,
  cb
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [
        {
          googleId: condition.id,
        },
        { email: condition.emails[0].value },
      ],
    },
    (err, result) => {
      if (result) {
        cb(err, result);
      } else {
        console.log("----- CONDITION -----");
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.userName = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("----- VALUES -----");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return cb(err, result);
        });
      }
    }
  );
};
UsuarioSchema.statics.findOrCreateByFacebook = function findOneOrCreate(
  condition,
  cb
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [
        {
          facebookId: condition.id,
        },
        { email: condition.emails[0].value },
      ],
    },
    (err, result) => {
      if (result) {
        cb(err, result);
      } else {
        console.log("----- CONDITION -----");
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.userName = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("----- VALUES -----");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) console.log(err);
          return cb(err, result);
        });
      }
    }
  );
};
module.exports = mongoose.model("Usuario", UsuarioSchema);
