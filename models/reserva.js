var mongoose = require("mongoose");
var moment = require("moment");
var Schema = mongoose.Schema;

var ReservaSchema = new Schema({
  usuario: { type: mongoose.Schema.Types.ObjectId, ref: "Usuario" },
  bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta" },
  inicio: Date,
  fin: Date,
  codigo: String,
});
ReservaSchema.methods.diasDeReserva = function () {
  return moment(this.fin).diff(moment(this.inicio), "days") + 1;
};
ReservaSchema.methods.toString = function () {
  return `Codigo: ${this.codigo} | Usuario: ${this.userName} | Bicicleta: ${this.bicicleta}`;
};

ReservaSchema.statics.createInstance = function (
  _userName,
  _bicicleta,
  _inicio,
  _fin,
  _codigo
) {
  return new this({
    userName: _userName,
    bicicleta: _bicicleta,
    inicio: _inicio,
    fin: _fin,
    codigo: _codigo,
  });
};

ReservaSchema.statics.allReservas = function (cb) {
  return this.find({}, cb);
};
ReservaSchema.statics.allByUser = function (userName, cb) {
  return this.find({ userName: userName }, cb);
};
ReservaSchema.statics.allByBicicleta = function (codigo, cb) {
  return this.find({ bicicleta: codigo }, cb);
};
ReservaSchema.statics.add = function (_reserva, cb) {
  this.create(_reserva, cb);
};
ReservaSchema.statics.findByUserName = function (_userName, cb) {
  return this.find({ userName: _userName }, cb);
};
ReservaSchema.statics.findByBicicleta = function (_codigoBicicleta, cb) {
  return this.find({ bicicleta: _codigoBicicleta }, cb);
};
ReservaSchema.statics.findByCodigo = function (_codigo, cb) {
  return this.find({ codigo: _codigo }, cb);
};

ReservaSchema.statics.editByCodigo = function (_codigo, _reserva, cb) {
  return this.updateOne({ codigo: _codigo }, _reserva, cb);
};

ReservaSchema.statics.removeByCodigo = function (_codigo, cb) {
  return this.deleteOne({ codigo: _codigo }, cb);
};
ReservaSchema.statics.removeByUsuario = function (_userName, cb) {
  return this.delete({ userName: _userName }, cb);
};

module.exports = mongoose.model("Reserva", ReservaSchema);
